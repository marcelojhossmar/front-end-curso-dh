// Tambien se podria deja  envez de class como interface
export class Item {
  name: string;
  image: string;
  category: string;
  label: string;
  price: string;
  description: string;
}
